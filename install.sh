#!/bin/bash

if [[ $SHELL != *"zsh" ]]; then
    if [[ -f "$(which zsh)" ]]; then
        git submodule init
        git submodule update
        echo "Chaning shell to zsh, please insert your password"
        chsh -s "$(which zsh)"
    fi
fi

# Installing dotfiles with stow

rm -rf "$HOME/.config/wezterm"
rm -rf "$HOME/.config/ghostty"
rm -rf "$HOME/.zshrc"
rm -rf "$HOME/.config/zsh"
rm -rf "$HOME/.config/zathura"

stow --dotfiles -t "$HOME/.config" -d wezterm dot-config
stow --dotfiles -t "$HOME/.config" -d ghostty dot-config
stow --dotfiles -t "$HOME/.config" -d zathura dot-config
stow --dotfiles -t "$HOME" zsh
stow --dotfiles -t "$HOME/.config" -d zsh-addons dot-config

if [ "$(command -v lspci)" ]; then
    if [[ $(lspci | grep -i vga) = *"Advanced Micro Devices"* ]]; then
        echo "Installing AMD profile for mpv"
        rm -rf "$HOME/.config/mpv"
        stow --dotfiles -t "$HOME/.config" -d mpv-amd dot-config
    elif [[ $(lspci | grep -i vga) = *"Intel Corporation"* ]]; then
        echo "Installing Intel profile for mpv"
        rm -rf "$HOME/.config/mpv"
        stow --dotfiles -t "$HOME/.config" -d mpv-intel dot-config
    elif [[ $(lspci | grep -i vga) = *"NVIDIA"* ]]; then
        echo "Installing NVIDIA profile for mpv"
        rm -rf "$HOME/.config/mpv"
        stow --dotfiles -t "$HOME/.config" -d mpv-nvidia dot-config
    else
        echo "No mpv profile for the current GPU, skipping"
    fi
else
    echo "lspci not found; skipping mpv profile"
fi
