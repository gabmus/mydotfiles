# GabMus's dotfiles

To install, make sure you have `stow` installed, then run:

```bash
./install.sh
```
