ZSH_CONFIG_DIR="$HOME/.config/zsh"
ZSH_PLUGINS_DIR="$ZSH_CONFIG_DIR/plugins"
ZSH_THEMES_DIR="$ZSH_CONFIG_DIR/themes"

source "$ZSH_CONFIG_DIR/completion.zsh"
source "$ZSH_CONFIG_DIR/history.zsh"
source "$ZSH_CONFIG_DIR/aliases.zsh"
source "$ZSH_CONFIG_DIR/magic_keybindings.zsh"

ZSH_PLUGINS=(zsh-syntax-highlighting)
# load plugins
for plugin ($ZSH_PLUGINS); do
    _plugin="$ZSH_PLUGINS_DIR/$plugin/$plugin.zsh"
    if [ ! -f "$_plugin" ]; then
        git -C $ZSH_CONFIG_DIR submodule init
        git -C $ZSH_CONFIG_DIR submodule update
    fi
    if [ -f "$_plugin" ]; then
        source "$_plugin"
    fi
    unset _plugin
done

# Changing/making/removing directory
setopt AUTO_CD
setopt AUTO_PUSHD
setopt PUSHD_IGNORE_DUPS
setopt PUSHD_MINUS
# ls colors
autoload -U colors && colors
# Enable ls colors
export LSCOLORS="Gxfxcxdxbxegedabagacad"
if [[ -z "$LS_COLORS" ]]; then
  (( $+commands[dircolors] )) && eval "$(dircolors -b)"
fi
zstyle ':completion:*' list-colors "${(s.:.)LS_COLORS}"

setopt MULTIOS
setopt PROMPT_SUBST
setopt INTERACTIVE_COMMENTS

# install a new package and have it available in the completion immediately
zstyle ':completion:*' rehash true

source "$ZSH_CONFIG_DIR/colorschemes/tokyonightstorm.zsh"

source "$ZSH_CONFIG_DIR/wezterm_integration.sh"
