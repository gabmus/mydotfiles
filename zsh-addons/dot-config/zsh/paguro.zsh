export PAGURO_SHELL=zsh

paguro_precmd() {
    export PAGURO_PREV_EXIT_CODE=$?
    export PAGURO_SUSPENDED_PROCESSES="$(jobs | wc -l)"
    PAGURO_NO_PROMPT_ESCAPE=1 PAGURO_COLUMNS=$COLUMNS paguro --preprompt
    PROMPT='$(PAGURO_COLUMNS=$COLUMNS paguro --prompt)'
}

if [ "$(command -v cargo)" ]; then
    if [ ! "$(command -v paguro)" ]; then
        if [ ! -f "$HOME/.cargo/bin/paguro" ]; then
            cargo install --git https://gitlab.com/gabmus/paguro
        else
            echo 'Please add $HOME/.cargo/bin to your PATH'
        fi
    fi
    autoload -Uz add-zsh-hook
    add-zsh-hook precmd paguro_precmd
else
    echo "Please install cargo on your system"
fi
