if [ "$TERM" = "linux" ]; then
    echo -en "\e]P024283b" #black
    echo -en "\e]P81D202F" #darkgrey
    echo -en "\e]P1f7768e" #darkred
    echo -en "\e]P9f7768e" #red
    echo -en "\e]P29ece6a" #darkgreen
    echo -en "\e]PA9ece6a" #green
    echo -en "\e]P3e0af68" #darkyellow
    echo -en "\e]PBe0af68" #yellow
    echo -en "\e]P47aa2f7" #darkblue
    echo -en "\e]PC7aa2f7" #blue
    echo -en "\e]P5bb9af7" #darkmagenta
    echo -en "\e]PDbb9af7" #magenta
    echo -en "\e]P67dcfff" #darkcyan
    echo -en "\e]PE7dcfff" #cyan
    echo -en "\e]P7a9b1d6" #lightgrey
    echo -en "\e]PFc0caf5" #white
    clear #for background artifacting
fi

