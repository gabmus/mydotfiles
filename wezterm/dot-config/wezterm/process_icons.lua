local W = require 'wezterm'

return function(theme) return {
    ["docker"] = {
        { Foreground = { Color = theme.ansi[5] } },
        { Text = W.nerdfonts.linux_docker },
    },
    ["docker-compose"] = {
        { Foreground = { Color = theme.ansi[5] } },
        { Text = W.nerdfonts.linux_docker },
    },
    ["nvim"] = {
        { Foreground = { Color = theme.ansi[4] } },
        { Text = W.nerdfonts.custom_vim },
    },
    ["vim"] = {
        { Foreground = { Color = theme.ansi[4] } },
        { Text = W.nerdfonts.dev_vim },
    },
    ["node"] = {
        { Foreground = { Color = theme.ansi[4] } },
        { Text = W.nerdfonts.mdi_hexagon },
    },
    ["zsh"] = {
        { Foreground = { Color = theme.ansi[6] } },
        { Text = W.nerdfonts.dev_terminal },
    },
    ["bash"] = {
        { Foreground = { Color = theme.ansi[6] } },
        { Text = W.nerdfonts.cod_terminal_bash },
    },
    ["paru"] = {
        { Foreground = { Color = theme.brights[6] } },
        { Text = W.nerdfonts.linux_archlinux },
    },
    ["htop"] = {
        { Foreground = { Color = theme.brights[4] } },
        { Text = W.nerdfonts.mdi_chart_donut_variant },
    },
    ["cargo"] = {
        { Foreground = { Color = theme.ansi[6] } },
        { Text = W.nerdfonts.dev_rust },
    },
    ["go"] = {
        { Foreground = { Color = theme.ansi[7] } },
        { Text = W.nerdfonts.mdi_language_go },
    },
    ["lazydocker"] = {
        { Foreground = { Color = theme.ansi[5] } },
        { Text = W.nerdfonts.linux_docker },
    },
    ["git"] = {
        { Foreground = { Color = theme.brights[2] } },
        { Text = W.nerdfonts.dev_git },
    },
    ["lazygit"] = {
        { Foreground = { Color = theme.brights[2] } },
        { Text = W.nerdfonts.dev_git },
    },
    ["lua"] = {
        { Foreground = { Color = theme.ansi[5] } },
        { Text = W.nerdfonts.seti_lua },
    },
    ["wget"] = {
        { Foreground = { Color = theme.brights[4] } },
        { Text = W.nerdfonts.mdi_arrow_down_box },
    },
    ["curl"] = {
        { Foreground = { Color = theme.brights[4] } },
        { Text = W.nerdfonts.mdi_flattr },
    },
    ["gh"] = {
        { Foreground = { Color = theme.brights[8] } },
        { Text = W.nerdfonts.dev_github_badge },
    },
} end
